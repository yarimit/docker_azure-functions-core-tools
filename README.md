# README

Azure Functions Core Tools (dotnet2.0.3 + azure-functions-core-tools@core)

### build

```
docker build -t arimitsu/azure-functions-core-tools .
```

### run

* Shell
```
docker run -it -v `pwd`:/home arimitsu/azure-functions-core-tools /bin/bash
```

